(function ($) {
    // country vs city pjax actions
    $(document).on('change', '.country-select', function (e) {
        var city_selector = $(this).attr('data-href');
        var id = $(this).val();
        if (document.querySelector(city_selector) !== null) {
            var _href = $(city_selector).attr('href');
            $(city_selector).attr('href', _href + '?country_id=' + id);
            $(city_selector).trigger('click');
        }
    });
})(jQuery);
