<?php

use yii\helpers\Url;
$this->registerJs('

    $("#bookingpack-sh_contact_name").autocomplete({
        lookup: ' . $sh_names . ',
        onSelect: function (suggestion) {
            
            $.ajax({
                url: "' . Url::to(['/booking/get-shipperdata']) . '",
                data: { sh_name : suggestion.value},
                success: function(data) {
                    data = JSON.parse(data);
                    $("#bookingpack-sh_type").val(data.type);
                    $("#bookingpack-sh_phone").val(data.phone);
                    $("#bookingpack-sh_email").val(data.email);
                    $("#bookingpack-sh_street1").val(data.street1);
                    $("#bookingpack-sh_street2").val(data.street2);
                    $("#bookingpack-sh_street3").val(data.street3);
                    $.ajax({
                        url: "' . Url::to(['/booking/get-postcodedata']) . '",
                        data: { postcode : data.service_area_id},
                        success: function(postcodeData) {
                            postcodeData = JSON.parse(postcodeData);
                            $("#country_select1").val(postcodeData.country_name).trigger("change");
                            state1 = postcodeData.state;
                            area1 = postcodeData.area;   
                        }
                    });
                },
          });
            
        }
    });
    
    $("#bookingpack-b_contact_name").autocomplete({
        lookup: '.$b_names.',
        onSelect: function (suggestion) {
            
            $.ajax({
                url: "' . Url::to(['/booking/get-receiverdata']) . '",
                data: { b_name : suggestion.value},
                success: function(data) {
                    data = JSON.parse(data);
                    $("#bookingpack-b_type").val(data.type);
                    $("#bookingpack-b_phone").val(data.phone);
                    $("#bookingpack-b_email").val(data.email);
                    $("#bookingpack-b_street1").val(data.street1);
                    $("#bookingpack-b_street2").val(data.street2);
                    $("#bookingpack-b_street3").val(data.street3);
                    $.ajax({
                        url: "' . Url::to(['/booking/get-postcodedata']) . '",
                        data: { postcode : data.service_area_id},
                        success: function(postcodeData) {
                            postcodeData = JSON.parse(postcodeData);
                            $("#country_select2").val(postcodeData.country_name).trigger("change");
                            state2 = postcodeData.state;
                            area2 = postcodeData.area;   
                        }
                    });
                },
          });
            
        }
    });

');