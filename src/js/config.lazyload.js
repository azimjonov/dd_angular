// lazyload config
var host = window.location.protocol+'//'+window.location.host;
// custom settings
if(window.location.host == 'projects.ukuya.net' || window.location.host == 'localhost' || window.location.host == '127.0.0.1') {
    host = host + '/dd';
}
var MODULE_CONFIG = {
    easyPieChart:   [ host+'/js/plugins/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ host+'/js/plugins/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ host+'/js/plugins/flot/jquery.flot.js',
                      host+'/js/plugins/flot/jquery.flot.resize.js',
                      host+'/js/plugins/flot/jquery.flot.pie.js',
                      host+'/js/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      host+'/js/plugins/flot-spline/js/jquery.flot.spline.min.js',
                      host+'/js/plugins/flot.orderbars/js/jquery.flot.orderBars.js'],
    vectorMap:      [ host+'/js/plugins/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      host+'/js/plugins/bower-jvectormap/jquery-jvectormap.css',
                      host+'/js/plugins/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      host+'/js/plugins/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
    dataTable:      [
                      host+'/js/plugins/datatables/media/js/jquery.dataTables.min.js',
                      host+'/js/plugins/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      host+'/js/plugins/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      host+'/js/plugins/footable/dist/footable.all.min.js',
                      host+'/js/plugins/footable/css/footable.core.css'
                    ],
    screenfull:     [
                      host+'/js/plugins/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      host+'/js/plugins/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      host+'/js/plugins/nestable/jquery.nestable.css',
                      host+'/js/plugins/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      host+'/js/plugins/summernote/dist/summernote.css',
                      host+'/js/plugins/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      host+'/js/plugins/parsleyjs/dist/parsley.css',
                      host+'/js/plugins/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      host+'/js/plugins/select2/dist/css/select2.min.css',
                      host+'/js/plugins/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      host+'/js/plugins/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      host+'/js/plugins/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      host+'/js/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      host+'/js/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      host+'/js/some/moment/moment.js',
                      host+'/js/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    chart:          [
                      host+'/js/some/echarts/build/dist/echarts-all.js',
                      host+'/js/some/echarts/build/dist/theme.js',
                      host+'/js/some/echarts/build/dist/jquery.echarts.js'
                    ],
    bootstrapWizard:[
                      host+'/js/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      host+'/js/plugins/moment/moment.js',
                      host+'/js/plugins/fullcalendar/dist/fullcalendar.min.js',
                      host+'/js/plugins/fullcalendar/dist/fullcalendar.css',
                      host+'/js/plugins/fullcalendar/dist/fullcalendar.theme.css',
                      host+'/js/plugins/calendar.js'
                    ],
    dropzone:       [
                      host+'/js/some/dropzone/dist/min/dropzone.min.js',
                      host+'/js/some/dropzone/dist/min/dropzone.min.css'
                    ]
  };
