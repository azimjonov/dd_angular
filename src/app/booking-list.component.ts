import { Component } from '@angular/core';

@Component({
    selector: 'booking-list',
    template: `
        <h2>{{title}}</h2>
    `,
})

export class BookingListComponent {
    title = 'BookingList';
}
