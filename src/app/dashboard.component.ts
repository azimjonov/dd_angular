import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl : './dashboard.component.html'
})

export class DashboardComponent {
    title = 'This is Dashboard';
}
