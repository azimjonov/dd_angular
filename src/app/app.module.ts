import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';

import { AppRoutingModule }     from './app-routing.module';
import {PagefirstComponent} from './pagefirst.component';
import {Page2Component} from './page-2.component';

import { BookingAddComponent } from './booking-add.component';
import { BookingComponent } from './booking.component';
import { BookingListComponent } from './booking-list.component';
import { DashboardComponent } from './dashboard.component';

import { LoginComponent } from './login.component';
import { SignupComponent } from './signup.component';


@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        PagefirstComponent,
        Page2Component,
        BookingListComponent,
        BookingAddComponent,
        BookingComponent,
        DashboardComponent,
        LoginComponent,
        SignupComponent
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
