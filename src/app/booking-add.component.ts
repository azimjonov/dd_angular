import { Component } from '@angular/core';

@Component({
    selector: 'booking-add',
    template: `
        <h2>{{title}}</h2>
        <div class="bad curly special" [class]="badCurly">Bad curly</div>
        <p><span>"{{evilTitle}}" is the <i>interpolated</i> evil title.</span></p>
        <p>"<span [innerHTML]="evilTitle"></span>" is the <i>property bound</i> evil title.</p>
    `,
})

export class BookingAddComponent {
    title = 'BookingAdd';
    evilTitle = 'Template <script>alert("evil never sleeps")</script>Syntax';
}
