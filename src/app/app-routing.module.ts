import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BookingComponent }   from './booking.component';

import { BookingAddComponent } from './booking-add.component';
import { BookingListComponent } from './booking-list.component';

import { DashboardComponent } from './dashboard.component';
import { LoginComponent } from './login.component';
import { SignupComponent } from './signup.component';

const routes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'booking',  component: BookingComponent,
        children: [
            { path: '', redirectTo: 'add', pathMatch: 'full' },
            { path: 'add', component: BookingAddComponent },
            { path: 'list', component: BookingListComponent}
        ]
    },
    { path: 'dashboard', component: DashboardComponent},
    { path: 'login', component: LoginComponent},
    { path: 'signup', component: SignupComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
